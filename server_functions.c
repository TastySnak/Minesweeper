#include <stdlib.h> 
#include <string.h> 
#include <arpa/inet.h>
#include <stdio.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <sys/wait.h> 
#include <unistd.h>
#include <errno.h>
#include <stdbool.h>
#include <omp.h>
#include <time.h>

#include "server.h"


/**********************************************/
/*              Program Functions             */
/**********************************************/

void graceful_shutdown() {

    /* close all sockets */
    close_sockets(players_list);

    /* Loop through players_list freeing each node */
    freePlayersList(players_list);

    /* Loop through games_list freeing each node */
    freeGamesList(games_list);

    /* Loop through creds_list freeing each node */
    freeCredsList(creds_list);

    /* Loop through scoreboard freeing each node */
    freeScoreboard(scoreboard);

    /* Loop through requests freeing each node */
    freeRequestList(requests);

    /* Cancel each of the threads in the threads array */
    cancel_threads();

    /* Destroy mutexes */
    destroy_mutexes();

    return;
}

/* Infinite loop of requests handling */
void * handle_requests_loop()
{
    request_t * new_req;

    /* lock the mutex, to access the requests list exclusively. */
    pthread_mutex_lock(&request_mutex);

    while (1) {
        if (num_requests > 0) {
            new_req = get_request(&request_mutex);
            if (new_req) {
                pthread_mutex_unlock(&request_mutex);

                handle_request(new_req);
                
                free(new_req);
                pthread_mutex_lock(&request_mutex);
            }
        }
        else {
            pthread_cond_wait(&got_request, &request_mutex);
        }
    }
}

/* handle a single given request */
void handle_request(request_t *next_req) {

    int newfd = next_req->number;
    int numbytes, counter = 0;    
    char buf[MAXDATASIZE];
    player_t *player = NULL;
    char *username, *password;

    /* Receives credentials as string from sign_in_user() function */
    if ((numbytes=recv(newfd, buf, MAXDATASIZE, 0)) == -1) {
        perror("recv");
        exit(1);
    }
    
    buf[numbytes] = '\0';

    /* Separate received string into username and password */
    char *token = strtok(buf, " ");

    // Keep printing tokens while one of the 
    // delimiters present in buf[]. 
    while (token != NULL) {
        
        switch (counter) {
            case 0:
                RemoveSpaces(token);
                username = token;
                counter++;
                break;
            case 1:
                RemoveSpaces(token);
                password = token;
                break;                
            default:
                break;
        }

        token = strtok(NULL, " ");
    }
    
    player = (player_t *) malloc (sizeof(player_t));

    /* Lock down the credentials list for reading */
    pthread_mutex_lock(&creds_mutex);

    player = authenticate_user(creds_list, player, username, password, newfd);

    /* Release the credentials list */
    pthread_mutex_unlock(&creds_mutex);

    if (player == NULL || player->name == NULL) {

        /* Authentication Failed */
        printf("Authentication Failed\n"); 

	    /* Convert to network byte order */
        uint16_t result = htons(0);

        if (send(newfd, &result, sizeof(uint16_t), 0) == -1)
            perror("send");
        close(newfd);

    } else {

        /* Authentication Successful */

        /* Lock down the players list */
        pthread_mutex_lock(&players_mutex);

        /* Add player to players_list */
        players_t *newhead  = add_player(player);
        if (newhead == NULL) {
            printf("memory allocation failure\n");
            
            exit(EXIT_FAILURE);
        }
        players_list = newhead;

        /* Release the players list */
        pthread_mutex_unlock(&players_mutex);
            
        printf("%s has joined the server\n", player->name);
        printf("Authentication Successful\n");
    
	    /* Convert to network byte order */
        uint16_t result = htons(1);

        if (send(newfd, &result, sizeof(uint16_t), 0) == -1)
            perror("send");
        
        /* Display the main menu */
        run_game(player);
    }
        
    /* Free player memory */
    free(player);
}

/* add a request to the requests list */
void add_request(int request_num,
            pthread_mutex_t* p_mutex,
            pthread_cond_t*  p_cond_var)
{
    request_t * new_req;

    /* create structure with new request */
    new_req = (request_t *)malloc(sizeof(request_t));
    if (!new_req) {
        fprintf(stderr, "add_request: out of memory\n");
        exit(1);
    }
    new_req->number = request_num;
    new_req->next = NULL;

    /* lock the mutex, to assure exclusive access to the list */
    pthread_mutex_lock(p_mutex);

    /* add new request to the end of the list */
    if (num_requests == 0) {
        requests = new_req;
        last_request = new_req;
    }
    else {
        last_request->next = new_req;
        last_request = new_req;
    }

    /* increase total number of pending requests by one. */
    num_requests++;

    /* unlock mutex */
    pthread_mutex_unlock(p_mutex);

    /* signal the condition variable - there's a new request to handle */
    pthread_cond_signal(p_cond_var);
}

/* gets the first pending request from the requests list removing it from the list. */
request_t * get_request(pthread_mutex_t* p_mutex) {

    request_t *next_req;

    /* lock the mutex, to assure exclusive access to the list */
    pthread_mutex_lock(p_mutex);

    if (num_requests > 0) {
        next_req = requests;
        requests = next_req->next;
        if (requests == NULL) {
            last_request = NULL;
        }
        /* decrease the total number of pending requests */
        num_requests--;
    }
    else {
        next_req = NULL;
    }

    /* unlock mutex */
    pthread_mutex_unlock(p_mutex);

    /* return the request to the caller. */
    return next_req;
}

/* Free the entire list */
void freeScoreboard(board_t *head) {
    board_t *temp;

    while (head != NULL) {
        temp = head;
        head = head->next;
        free(temp);
    }

    return;
}

/* Free the entire list */
void freeCredsList(client_t *head) {
    client_t *temp;

    while (head != NULL) {
        temp = head;
        head = head->next;
        free(temp->user);
        free(temp->pass);
        free(temp);
    }

    return;
}


/* Free the entire list */
void freeRequestList(request_t *head) {
    request_t *temp;

    while (head != NULL) {
        temp = head;
        head = head->next;
        free(temp);
    }

    return;
}

/* Free the entire list */
void freePlayersList(players_t *head) {
    players_t *temp;

    while (head != NULL) {
        temp = head;
        head = head->next;
        free(temp);
    }

    return;
}

/* Free the entire list */
void freeGamesList(games_t *head) {
    games_t *temp;

    while (head != NULL) {
        temp = head;
        head = head->next;
        free(temp);
    }

    return;
}

/* Close all sockets */
void close_sockets(players_t *node) {

    while (node != NULL) {
        printf("Closing node: %d\n", node->player.newfd);
        close(node->player.newfd);
        node = node->next;
    }

    close(new_fd);
    close(sockfd);

    return;
}

/* Destroy all the nutexes */
void destroy_mutexes() {
    pthread_mutex_destroy(&rand_mutex);
    pthread_mutex_destroy(&request_mutex);
    pthread_mutex_destroy(&read_count_mutex);
    pthread_mutex_destroy(&rw_leader_mutex);
    pthread_mutex_destroy(&players_mutex);
    pthread_mutex_destroy(&games_mutex);
    pthread_mutex_destroy(&creds_mutex);
    pthread_mutex_destroy(&threads_mutex);

    return;
}

/* Cancel all the threads in the threads array */
void cancel_threads() {

    for (int index = 0; index < BACKLOG; index++) {
        
        pthread_mutex_lock(&threads_mutex);

        if (pthread_cancel(p_threads[index]) != 0) {
            perror("pthread_cancel() error");
            exit(1);
        }

        if (pthread_attr_destroy(&t_attr[index]) != 0) {
            perror("pthread_attr_destroy() error");
            exit(1);
        }
        
        pthread_mutex_unlock(&threads_mutex);
    }

    return;
}

/* Signal handler to free allocated memory */
void sigHandler(int sig) {
    
    /* Graceful close after SIGINT */
    printf("\nInitialising graceful shutdown...\n");

    graceful_shutdown();
    exit(0);

    return;
}

/* Lets the client know the server is ready to receive more data */
void prompt_client(int newfd) {

    if (send(newfd, "a", 1, 0) == -1)
        perror("send");

    return;
}

/* Receives client's request to receive more data */
void receive_prompt(int newfd) {
    
    char ch;
    int numbytes;
    
    if ((numbytes=recv(newfd, &ch, 1, 0)) == -1) {
        perror("recv");
        exit(1);
    }

    return;
}
    
/* Checks to see if tile already has been assigned a mine */
/* If not, a mine will be assigned to it and return false */
/* If the tile already has a mine assigned to it, return true */
bool check_and_assign_mine(game_t *game, int mine_row, int mine_col) {

    if (game->game_board[mine_row][mine_col].has_mine)
        return true;

    game->game_board[mine_row][mine_col].has_mine = true;

    return false;
}

/* Initialises the adjacent_mines field by checking all adjacent tiles for mines */
void initialise_adjacent_mines_field(game_t *game) {

    for (int row = 0; row < NUM_TILES_Y; row++) {
        for (int column = 0; column < NUM_TILES_X; column++) {

            /* Disregard all tiles that have mines */
            if (game->game_board[row][column].has_mine) {
                continue;
            }

                /* Check tile to the above left */
            if (!(((row - 1) < 0) || ((column - 1) < 0)) &&             // Check if will be out of scope
                game->game_board[row - 1][column - 1].has_mine) {       // Check if has mine
                    game->game_board[row][column].adjacent_mines++;
            }

            /* Check tile above */
            if (!((row - 1) < 0) &&
                game->game_board[row - 1][column].has_mine) {
                    game->game_board[row][column].adjacent_mines++;
            }

            /* Check tile to the above right */        
            if (!(((row - 1) < 0) || ((column + 1) >= NUM_TILES_X)) &&
                game->game_board[row - 1][column + 1].has_mine) {
                    game->game_board[row][column].adjacent_mines++;
            }

            /* Check tile to the left */           
            if (!((column - 1) < 0) &&
                game->game_board[row][column - 1].has_mine) {
                    game->game_board[row][column].adjacent_mines++;
            }

            /* Check tile to the right */
            if (!((column + 1) >= NUM_TILES_X) &&
                game->game_board[row][column + 1].has_mine) {
                    game->game_board[row][column].adjacent_mines++;
            }

            /* Check tile to the below left */
            if (!(((row + 1) >= NUM_TILES_Y) || ((column - 1) < 0)) &&
                game->game_board[row + 1][column - 1].has_mine) {
                    game->game_board[row][column].adjacent_mines++;
            }

            /* Check tile below */
            if (!((row + 1) >= NUM_TILES_Y) &&
                game->game_board[row + 1][column].has_mine) {
                    game->game_board[row][column].adjacent_mines++;
            }

            /* Check tile to the below right */
            if (!(((row + 1) >= NUM_TILES_Y) || ((column + 1) >= NUM_TILES_X)) &&
                game->game_board[row + 1][column + 1].has_mine) {
                    game->game_board[row][column].adjacent_mines++;
            }      
        }
    }

    return;
}

/* Generates the location of each mine on the gameboard */
void place_mines(game_t *game) {

    int mine_row, mine_col;

    /* Since rand is not thread safe, it needs to be secured with a mutex */
    pthread_mutex_lock(&rand_mutex);

    for (int mines = 0; mines < NUM_MINES; mines++) {
        do {
            mine_row = rand() % NUM_TILES_Y;
            mine_col = rand() % NUM_TILES_X;
        } while (check_and_assign_mine(game, mine_row, mine_col));
    }

    pthread_mutex_unlock(&rand_mutex);

    return;
}

/* Initialises the game state at the beginning of each game */
void initialise_gamestate(game_t *game) {

    /* Assign variables within struct */
    game->mines_remaining = NUM_MINES;

    for (int row = 0; row < NUM_TILES_Y; row++) {
        for (int column = 0; column < NUM_TILES_X; column++) {
            game->game_board[row][column].adjacent_mines = 0;
            game->game_board[row][column].unknown = true;
            game->game_board[row][column].has_flag = false;
            game->game_board[row][column].has_mine = false;
        }
    }

    /* Generate the mines and assign them to a tile */
    place_mines(game);

    /* For all tiles not contailing a mine */
    /* find how many surrounding tiles contain mines */
    /* and assign the value to the tile */
    initialise_adjacent_mines_field(game);

    return;
}

/* Sends gameboard data to Client */
void send_game_board(player_t *player, game_t *game, bool game_on) {

    char tile[NUM_TILES_Y * NUM_TILES_X];
    int counter = 0, newfd = player->newfd;
    uint16_t mines;

    /* Send the number of remaining mines to the client in network byte order */
    mines = htons(game->mines_remaining);

    if (send(newfd, &mines, sizeof(uint16_t), 0) == -1)
        perror("send");

    /* Client is ready for next node's data on receipt */
    receive_prompt(newfd);

    /* Assign gameboard to array */    
    if (game_on) {
        /* if game is still in play */
        for (int row = 0; row < NUM_TILES_Y; row++) {
            for (int col = 0; col < NUM_TILES_X; col++) {

                if (game->game_board[row][col].has_flag) {
                    tile[counter] = '+';

                } else if (!(game->game_board[row][col].unknown)) {

                    if (game->game_board[row][col].adjacent_mines == 0)
                        tile[counter] = '-';
                    else
                        // adding '0' converts the integer to the corresponding character
                        tile[counter] = game->game_board[row][col].adjacent_mines + '0';
                } else {
                    tile[counter] = ' ';
                }
                counter++;            
            }
        }
    } else {
        /* If game has been won, reveal all tiles */
        /* Else the game is lost, reveal mines only */
        for (int row = 0; row < NUM_TILES_Y; row++) {
            for (int col = 0; col < NUM_TILES_X; col++) {
                if (player->has_won) {
                    if (game->game_board[row][col].has_flag) {
                        tile[counter] = '+';

                    } else if (game->game_board[row][col].adjacent_mines == 0) {
                        tile[counter] = '-';
                    } else {
                        // adding '0' converts the integer to the corresponding character
                        tile[counter] = game->game_board[row][col].adjacent_mines + '0'; 
                    }
                            
                } else {                    
                    if (game->game_board[row][col].has_mine) {
                        tile[counter] = '*';
                    } else {
                        tile[counter] = ' ';
                    }
                }
                counter++;           
            }
        }
    }

    /* Send finished array to client */
    for (int index = 0; index < (NUM_TILES_Y * NUM_TILES_X); index++) {
		send(newfd, &tile[index], 1, 0);
	}

    return;
}

/* End game */
bool end_game(player_t *player) {

    /* Request user input */
    prompt_client(player->newfd);

    /* Increment the number of games played */
    player->games_played++;

    /* update leaderboard if player is in it */
    update_leaderboard(player);

    return false;
}

/* Update the leaderboard */
void update_leaderboard(player_t *player) {
    
    pthread_mutex_lock(&rw_leader_mutex);

    board_t *head = scoreboard;

    /* If player already in leaderboard, update games_played */
    for ( ; scoreboard != NULL; scoreboard = scoreboard->next) {
        if (strcmp(scoreboard->name, player->name) == 0) {
            scoreboard->games_played = player->games_played;
            scoreboard->games_won = player->games_won;
        }
    }

    /* Reset scoreboard head */
    scoreboard = head;

    if (player->has_won) {

        /* Initialise data for new node */
        board_t new;

        new.name = player->name;
        new.game_time = player->game_time;
        new.games_played = player->games_played;
        new.games_won = player->games_won;
        
        /* Add new node to leaderboard */
        board_t *new_node = add_to_leaderboard(&new);
        if (new_node == NULL) {
            printf("memory allocation failure\n");

            exit(EXIT_FAILURE);
        }

        scoreboard = new_node;
    }

    pthread_mutex_unlock(&rw_leader_mutex);

    return;
}

/* Check entered coordinates for mine */
bool check_for_mine(game_t *game, player_t *player, int *coords, int start_time) {

    int newfd = player->newfd;

    /* As the tile has already been revealed and no mine at location both return true */
    /* This data structure enables the client to distinguish between the two */
    /* First character is whether the tile has already been revealed ['R'/'H'] */
    /* Second character is whether the tile has a mine ['T'/'F'] */
    /* Third character is whether there are mines remaining ['Y'/'N'] */
    char results[RESULTS_SIZE];

    /* Check if tile has already been revealed and send results to client */
    if (!(game->game_board[coords[ROW]][coords[COLUMN]].unknown)) {

        results[0] = 'R';
        results[1] = 'F';
        results[2] = 'Y';

        for (int index = 0; index < RESULTS_SIZE; index++) {
            send(newfd, &results[index], 1, 0);
        }

        return true;
    }

    /* Check if tile contains a mine and send results to client */
    if (game->game_board[coords[ROW]][coords[COLUMN]].has_mine) {
        game->mines_remaining--;
        game->game_board[coords[ROW]][coords[COLUMN]].has_flag = true;
        game->game_board[coords[ROW]][coords[COLUMN]].unknown = false;

        results[0] = 'H';
        results[1] = 'T';
        results[2] = 'Y';

        /* Check if player has flagged all the mines */
        if (game->mines_remaining < 1) {

            /* Get game finish time */
            int end_time = omp_get_wtime();

            /* Increment the number of games played */
            player->games_played++;
    
            /* Increment the number of games won */
            player->games_won++;

            /* Adjust has_won variable */
            player->has_won = true;

            /* Calculate win time */
            player->game_time = end_time - start_time;

            /* Add to leaderboard */
            update_leaderboard(player);

            /* Assign results */
            results[2] = 'N';

            /* Send results to client */
            for (int index = 0; index < RESULTS_SIZE; index++) {
                send(newfd, &results[index], 1, 0);
            }

            return false;
        }

    } else {
        /* Tile does not have a mine */

        /* Assign results */
        results[0] = 'H';
        results[1] = 'F';
        results[2] = 'Y';
    }
    
    /* Send results to client */
    for (int index = 0; index < RESULTS_SIZE; index++) {
        send(newfd, &results[index], 1, 0);
    }

    return true;
}

/* place flag at designated coordinates if mine exists there */
bool place_flag(game_t *game, player_t *player, int start_time) {

    int coords[NUM_COORDS];
    uint16_t coord;
    int numbytes, newfd = player->newfd;

    /* Receive coordinates from client's place_flag() function */
    for (int index = 0; index < 2; index++) {

		if ((numbytes=recv(newfd, &coord, sizeof(uint16_t), 0))
		         == RETURNED_ERROR) {
			perror("recv");
			exit(EXIT_FAILURE);			
		    
		}

	    /* Convert to host byte order */
		coords[index] = ntohs(coord);
	}

    /* Check tile for mine*/
    return check_for_mine(game, player, coords, start_time);
}

/* Reveal the user selected tile */
bool reveal_tile(game_t *game, player_t *player) {

    int coords[NUM_COORDS], newfd = player->newfd;
    uint16_t coord;
    int numbytes;

    /* Receive coordinates from client's reveal_tile() function */
    for (int index = 0; index < 2; index++) {
		if ((numbytes=recv(newfd, &coord, sizeof(uint16_t), 0))
		         == RETURNED_ERROR) {
			perror("recv");
			exit(EXIT_FAILURE);			
		    
		}
        
	    /* Convert to host byte order */
		coords[index] = ntohs(coord);
	}

    /* Check the status of the tile */
    return check_tile_status(game, player, coords);
}

/* Check the user's input against the tiles on the gameboard */
bool check_tile_status(game_t *game, player_t *player, int *coords) {

    /* This is a recursive function */
    /* Check for base cases */

    /* Check for mine */
    if (game->game_board[coords[ROW]][coords[COLUMN]].has_mine) {

        /* Increment the number of games played */
        player->games_played++;

        /* Update leaderboard if player exists on it */
        update_leaderboard(player);

        return false;
    }

    /* Check if tile has already been revealed */
    if (!(game->game_board[coords[ROW]][coords[COLUMN]].unknown)) {

        return true;
    }

    /* Check if adjacent to mine */
    if (game->game_board[coords[ROW]][coords[COLUMN]].adjacent_mines > 0) {
        game->game_board[coords[ROW]][coords[COLUMN]].unknown = false;

        return true;
    }

    /* Reveal tile */
    game->game_board[coords[ROW]][coords[COLUMN]].unknown = false;
    
    int indices[NUM_COORDS];

    /* Recursively check surrounding tiles */
    /* Get neighbouring tile coordinates */
    for (int neighbour = 0; neighbour < 8; neighbour++) {
        
        switch (neighbour) {
            case 0:
                indices[ROW] = coords[ROW] - 1;
                indices[COLUMN] = coords[COLUMN] - 1;

                /* Check that tile is not out of bounds */
                if (check_range(indices)) {
                        break;
                }

                check_tile_status(game, player, indices);

                break;
        
            case 1:
                indices[ROW] = coords[ROW] - 1;
                indices[COLUMN] = coords[COLUMN];

                /* Check that tile is not out of bounds */
                if (check_range(indices)) {
                        break;
                }

                check_tile_status(game, player, indices);

                break;
        
            case 2:
                indices[ROW] = coords[ROW] - 1;
                indices[COLUMN] = coords[COLUMN] + 1;

                /* Check that tile is not out of bounds */
                if (check_range(indices)) {
                        break;
                }

                check_tile_status(game, player, indices);

                break;
        
            case 3:
                indices[ROW] = coords[ROW];
                indices[COLUMN] = coords[COLUMN] - 1;

                /* Check that tile is not out of bounds */
                if (check_range(indices)) {
                        break;
                }

                check_tile_status(game, player, indices);

                break;
        
            case 4:
                indices[ROW] = coords[ROW];
                indices[COLUMN] = coords[COLUMN] + 1;

                /* Check that tile is not out of bounds */
                if (check_range(indices)) {
                        break;
                }

                check_tile_status(game, player, indices);

                break;
        
            case 5:
                indices[ROW] = coords[ROW] + 1;
                indices[COLUMN] = coords[COLUMN] - 1;

                /* Check that tile is not out of bounds */
                if (check_range(indices)) {
                        break;
                }

                check_tile_status(game, player, indices);

                break;
        
            case 6:
                indices[ROW] = coords[ROW] + 1;
                indices[COLUMN] = coords[COLUMN];

                /* Check that tile is not out of bounds */
                if (check_range(indices)) {
                        break;
                }

                check_tile_status(game, player, indices);

                break;
        
            case 7:
                indices[ROW] = coords[ROW] + 1;
                indices[COLUMN] = coords[COLUMN] + 1;

                /* Check that tile is not out of bounds */
                if (check_range(indices)) {
                        break;
                }

                check_tile_status(game, player, indices);

                break;
        
            default:
                break;
        }
    }

    return true;
}

/* Play Minesweeper */
void play_game(player_t *player) {

    /* Set up game */
    bool game_on = true;
    char game_choice, result = 'F';
    int start_time = omp_get_wtime();
    int numbytes, newfd = player->newfd;
    
    /* Allocate memory for game struct */
    game_t *game = malloc(sizeof(game_t));

    /* Lock down the games list */
    pthread_mutex_lock(&games_mutex);

    /* Add game to games list */
    games_t *newhead = add_game(games_list, game);
    if (newhead == NULL) {
        printf("memory allocation failure\n");
        
        exit(EXIT_FAILURE);
    }
    games_list = newhead;
    
    /* Release the games list */
    pthread_mutex_unlock(&games_mutex);

    /* Assign initial game variables */
    initialise_gamestate(game);
    
    /* Do while game is in play */
    do {
        /* Update play screen on client */
        send_game_board(player, game, game_on);

        /* Receive game menu choice from client */
        if ((numbytes=recv(newfd, &game_choice, 1, 0)) == -1) {
            perror("recv");
            exit(1);
        }

        switch (game_choice) {
            case 'R':
            case 'r':
                game_on = reveal_tile(game, player);

                /* If a mine was revealed */
                if (!game_on) {
                    result = 'T';
                } 

                /* Send result to the client*/
                if (send(newfd, &result, 1, 0) == -1)
                    perror("send");

                /* Client is ready to be sent play screen on receipt */
                receive_prompt(newfd);

                /* Update play screen on client */
                send_game_board(player, game, game_on);

                break;

            case 'P':
            case 'p':
                game_on = place_flag(game, player, start_time);

                /* Receive request from the client to send the next lot of data */
                receive_prompt(newfd);
                break;

            case 'Q':
            case 'q':
                game_on = end_game(player);
                break;
        
            default:
                break;
        }
    } while (game_on);

    /* Send final game board results on win */
    if (game_choice == 'P' || game_choice == 'p') {

        /* Update play screen on client */
        send_game_board(player, game, game_on);

        /* Receive request from the client to send the next lot of data */
        receive_prompt(newfd);

	    /* Convert to network byte order */
        uint16_t g_time = htons(player->game_time);

        if (send(newfd, &g_time, sizeof(uint16_t), 0) == -1)
            perror("send");
    }

    /* Free game memory */
    free(game);
    
    return;
}

/* Swapping data of two leaderboard nodes*/
void swap(board_t *first, board_t *second) 
{ 
    char *temp_name;
    int temp_game_time;
    int temp_games_played;
    int temp_games_won;

    temp_name = first->name; 
    temp_game_time = first->game_time;
    temp_games_played = first->games_played;
    temp_games_won = first->games_won;

    first->name = second->name; 
    first->game_time = second->game_time;
    first->games_played = second->games_played;
    first->games_won = second->games_won;

    second->name = temp_name; 
    second->game_time = temp_game_time;
    second->games_played = temp_games_played;
    second->games_won = temp_games_won;

    return;
} 

/* Bubble sort the given linked list */
void sort_leaderboard(board_t *head) 
{ 
    int swapped; 
    board_t *ptr = NULL; 
    board_t *lptr = NULL; 
  
    /* Checking for empty list */
    if (head == NULL) 
        return; 
  
    do { 
        swapped = 0; 
        ptr = head; 
  
        while (ptr->next != lptr) 
        {
            if ((ptr->game_time == ptr->next->game_time &&
                ptr->games_won == ptr->next->games_won &&
                ptr->name < ptr->next->name) ||

                (ptr->game_time == ptr->next->game_time &&
                ptr->games_won > ptr->next->games_won) ||

                (ptr->game_time < ptr->next->game_time))
            {  
                swap(ptr, ptr->next); 
                swapped = 1; 
            } 
            ptr = ptr->next; 
        } 
        lptr = ptr; 
    } 
    while (swapped);

    return;
}

/* Send the leaderboard to the client node by node */
void send_leaderboard(int newfd) {

    board_t *head = scoreboard;

    /* Convert the linked list to an array */
    for ( ; scoreboard != NULL; scoreboard = scoreboard->next) {
        
        /* Receive ready signal from client */
        receive_prompt(newfd);
        
        char number1[MAX_NUMBER_LENGTH];
        char number2[MAX_NUMBER_LENGTH];
        char number3[MAX_NUMBER_LENGTH];

        char *arr;
        arr = malloc(MAX_ARRAY_SIZE);
        memset(arr, 0, MAX_ARRAY_SIZE);

        strcpy(arr, scoreboard->name);
        strcat(arr, " ");
        sprintf(number1, "%d", scoreboard->games_played);
        
        strcat(arr, number1);
        strcat(arr, " ");
        sprintf(number2, "%d", scoreboard->games_won);
        
        strcat(arr, number2);
        strcat(arr, " ");
        sprintf(number3, "%d", scoreboard->game_time);
        
        strcat(arr, number3);
        strcat(arr, "\0");

        /* Send node data to client */
        if (send(newfd, arr, strlen(arr) + 1, 0) == -1)
			perror("send");

        /* Free array node memory */
        free(arr);
    }

    /* Return pointer to the head of list */
    scoreboard = head;

    return;
}

/* Counts the number of nodes in the leaderboard to be sent */
int count_nodes(int newfd) {

    int count = 0;
    board_t *head = scoreboard;

    while (scoreboard != NULL) {
        count++;

        scoreboard = scoreboard->next;
    }

    /* Return pointer to the head of list */
    scoreboard = head;

    /* Send number of nodes to client in network byte order */ 
    uint16_t node_count = htons(count);
	send(newfd, &node_count, sizeof(uint16_t), 0);

    return count;
}

/* Add new ranking to the leaderboard */
board_t * add_to_leaderboard(board_t *head) {

    /* create new nose to add to the list */
    board_t *new = (board_t *)malloc(sizeof(board_t));
    if (new == NULL) {
        return NULL;
    }

    /* insert new node */
    new->name = head->name;
    new->game_time = head->game_time;
    new->games_played = head->games_played;
    new->games_won = head->games_won;

    new->next = scoreboard;

    return new;
}

/* Counts the number of nodes in the leaderboard to be sent */
player_t * check_leaderboard(player_t * player) {
    board_t *head = scoreboard;

    while (scoreboard != NULL) {
        if (strcmp(scoreboard->name, player->name) == 0) {
            player->games_played = scoreboard->games_played;
            player->games_won = scoreboard->games_won;

            break;
        }

        scoreboard = scoreboard->next;
    }

    scoreboard = head;

    return player;
}

/* Main Menu */
void run_game(player_t *player) {

    bool quit_game = false;
    int numbytes, main_choice, newfd = player->newfd;
    uint16_t menu_choice;

    /* Do while user still wants to play game */
    do {
        /* Initialise player' game variables */
        player->has_won = false;
        player->game_time = 0;

        /* Receive user's choice from client's main_menu() function */
        if ((numbytes=recv(newfd, &menu_choice, sizeof(uint16_t), 0)) == -1) {
            perror("recv");
            exit(1);
        }

        /* Convert to host byte order */
        main_choice = ntohs(menu_choice);

        switch (main_choice) {
            case 1:
                play_game(player);
                
                break;

            case 2:
                show_leaderboard(newfd);
                
                break;

            case 3:
                remove_from_players_list(player);
                quit_game = quit(newfd);
                
                break;
        
            default:
                break;
        }
    } while (!quit_game);
    
    return;
}

/* Send the leaderboard to the client */
void show_leaderboard(int newfd) {

    /* Protect the read_count variable */
    pthread_mutex_lock(&read_count_mutex);

    read_count++;

    /* If first reader to enter the critical section */
    if (read_count == 1)
        pthread_mutex_lock(&rw_leader_mutex);

    /* Release read_count variable */
    pthread_mutex_unlock(&read_count_mutex);

    if (count_nodes(newfd)) {

        sort_leaderboard(scoreboard);
        send_leaderboard(newfd);
    }
    
    /* Protect the read_count variable */
    pthread_mutex_lock(&read_count_mutex);
                
    read_count--;

    /* If last reader to enter the critical section */
    if (read_count == 0)
        pthread_mutex_unlock(&rw_leader_mutex);

    /* Release read_count variable */
    pthread_mutex_unlock(&read_count_mutex);

    return;
}

/* Exit program */
bool quit(int newfd) {

	/* Convert to network byte order */
    uint16_t result = htons(QUIT);

    if (send(newfd, &result, sizeof(uint16_t), 0) == -1)
        perror("send");

    return true;
}

/* User authentication */
player_t * authenticate_user(client_t *creds_list, player_t *player, char *username, char *password, int newfd) {

    client_t *head = creds_list;
    players_t *p_head = players_list;

    /* Check to see if player already logged in */
    for ( ; players_list != NULL; players_list = players_list->next) {
        if ((strcmp(username, players_list->player.name) == 0)) {
            
            /* return pointer to the head of list*/
            players_list = p_head;

            return NULL;
        }
    }

    /* return pointer to the head of list*/
    players_list = p_head;

    /* Check that user can authenticate */
    for ( ; creds_list != NULL; creds_list = creds_list->next) {
        if ((strcmp(username, creds_list->user) == 0) && 
            (strcmp(password, creds_list->pass) == 0)) {
            
            /* Initialise Player struct on successful log in */
            player->newfd = newfd;
            strncpy(player->name, username, sizeof(player->name));
            player->games_played = 0;
            player->games_won = 0;
            player->game_time = 0;

            /* Check to see if they have old stats */
            player = check_leaderboard(player);

            printf("Name: %s\nGames played: %d\nGames won: %d\n", player->name, player->games_played, player->games_won);

            /* return pointer to the head of list*/
            creds_list = head;

            return player;
        }
    }

    /* return pointer to the head */
    creds_list = head;

    return NULL;
}

/* Add authenticated player to the list of players */
players_t * add_player(player_t *player) {

    /* Create a new node to add to the list */
    players_t *new = (players_t *) malloc (sizeof(players_t));
    if (new == NULL) {
        return NULL;
    }

    /* Insert new node */
    new->player = *player;

    new->next = players_list;
    new->prev = NULL;

    if (players_list != NULL)
        new->next->prev = new;

    return new;
}

void remove_from_players_list(player_t *player) {

    players_t *head = players_list;
    players_t *temp;

    while (players_list != NULL) {

        /* If node to be deleted is head node */
        if ((strcmp(head->player.name, player->name) == 0)) {
            temp = head;
            head = head->next;

            free(temp);

            break;
        }

        if ((strcmp(players_list->player.name, player->name) == 0)) {

            /* If node to be deleted is last node */
            if (players_list->next == NULL) {
                
                temp = players_list;
                temp->prev->next = NULL;

                free(temp);

                break;
            }

            /* If node to be deleted is not on fringe of list */
            temp = players_list;
            temp->prev->next = temp->next;
            temp->next->prev = temp->prev;

            free(temp);

            break;
        }

        players_list = players_list->next;
    }

    players_list = head;

    return;
}


/* Add game to the list of games */
games_t * add_game(games_t *g_list, game_t *game) {

    /* Create a new node to add to the list */
    games_t *new = (games_t *) malloc (sizeof(games_t));
    if (new == NULL) {
        return NULL;
    }

    /* Insert new node */
    new->game = *game;
    new->next = g_list;

    return new;
}

/* Retrieves all user credentials from file and stores them in a list */
client_t * parse_authentication_file(client_t *creds_list) {
    FILE * fp;
    char buffer[MAX_ARRAY_SIZE];

    fp = fopen("Authentication.txt", "r");

    /* skip first line of file */
    fscanf(fp, "%*[^\n]\n");

    while (fscanf(fp, "%[^\n]\n", buffer) != EOF) {
        int counter = 0;

        char *token = strtok(buffer, "\t"); 
        
        /* Create new user */
        client_t *newhead= (client_t *)malloc(sizeof(client_t));
        if (newhead == NULL) {
            return NULL;
        }

        // Keep printing tokens while one of the 
        // delimiters present in buffer[]. 
        while (token != NULL) {
            
            switch (counter) {
                case 0:
                    RemoveSpaces(token);
                    newhead->user = strdup(token);
                    counter++;
                    break;
                case 1:
                    RemoveSpaces(token);
                    newhead->pass = strdup(token);
                    break;                
                default:
                    break;
            }

            token = strtok(NULL, "\t");
        }

        newhead->next = creds_list;

        creds_list = newhead;
    }
    
    fclose(fp);

    return creds_list;
}

/* Removes the spaces from strings */
void RemoveSpaces(char* string) {
    int count = 0;

    for (int index = 0; string[index]; index++) {
        if (string[index] != ' ')
            string[count++] = string[index];
    }

    /* Place null terminator at the end of the character array */
    string[count] = '\0';

    return;
}

/* Check the coordinates are within the range of the gameboard */
bool check_range(int *indices) { 
    if ((indices[ROW] < 0) ||
        (indices[ROW] > (NUM_TILES_Y - 1)) ||
        (indices[COLUMN] < 0) ||
        (indices[COLUMN] > (NUM_TILES_X - 1))) {

            return true;
        }

    return false;
}