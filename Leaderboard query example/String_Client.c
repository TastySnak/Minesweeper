#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <string.h> 
#include <netdb.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <unistd.h>

	#define PORT 54320    /* the port client will be connecting to */

	#define MAXDATASIZE 20 /* max number of bytes we can get at once */

/* Removes the spaces from strings */
void RemoveSpaces(char* string) {
    int count = 0;

    for (int index = 0; string[index]; index++) {
        if (string[index] != ' ')
            string[count++] = string[index];
    }

    string[count] = '\0';

    return;
}

void print_scores(int node_count, int sockfd) {
	int numbytes;
	char buf[MAXDATASIZE];

	for (int index = 0; index < node_count; index++) {

		if ((numbytes=recv(sockfd, buf, MAXDATASIZE, 0)) == -1) {
            perror("recv");
            exit(1);
        }

		char *token = strtok(buf, " "); 
		char  *name, *game_time, *games_played, *games_won;
		int counter = 0;

        // Keep printing tokens while one of the 
        // delimiters present in str[]. 
        while (token != NULL) {
            
            switch (counter) {
                case 0:
                    RemoveSpaces(token);
                    name = token;
                    counter++;
                    break;
                case 1:
                    RemoveSpaces(token);
                    games_played = token;
                    counter++;
                    break;     
                case 2:
                    RemoveSpaces(token);
                    games_won = token;
                    counter++;
                    break;     
                case 3:
                    RemoveSpaces(token);
                    game_time = token;
                    counter++;
                    break;                
                default:
                    break;
            }

            token = strtok(NULL, " ");
        }

		printf(" %-10s %10s seconds %10s games won, %2s games played\n", name, game_time, games_won, games_played);

		if (send(sockfd, "a", 1, 0) == -1)
					perror("send");
	}

	return;
}

int main(int argc, char *argv[])
{
	int sockfd, numbytes, node_count;  
	char buf[MAXDATASIZE];
	struct hostent *he;
	struct sockaddr_in their_addr; /* connector's address information */
	uint16_t n_count;

	if (argc != 2) {
		fprintf(stderr,"usage: client hostname\n");
		exit(1);
	}

	if ((he=gethostbyname(argv[1])) == NULL) {  /* get the host info */
		herror("gethostbyname");
		exit(1);
	}

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(1);
	}

	their_addr.sin_family = AF_INET;      /* host byte order */
	their_addr.sin_port = htons(PORT);    /* short, network byte order */
	their_addr.sin_addr = *((struct in_addr *)he->h_addr);
	bzero(&(their_addr.sin_zero), 8);     /* zero the rest of the struct */

	if (connect(sockfd, (struct sockaddr *)&their_addr, \
	sizeof(struct sockaddr)) == -1) {
		perror("connect");
		exit(1);
	}




	


	/* Receive user's Main Menu choice */
	if ((numbytes=recv(sockfd, &n_count, sizeof(uint16_t), 0)) == -1) {
		perror("recv");
		exit(1);
	}

	printf("Main Menu choice received\n");

	/* Convert to host byte order */
	node_count = ntohs(n_count);

	if (node_count) {
        printf("\n\n=====================================================================\n");
        printf("=====================  Minesweeper Leaderboard  =====================\n");
        printf("=====================================================================\n\n");
		
        /* Print the sorted leaderboard */
        print_scores(node_count, sockfd);

        printf("\n=====================================================================\n\n");
        

	} else {
		
        printf("\n\n==============================================================================\n\n");
        printf("There is no information currently stored in the leaderboard.  Try again later.\n\n");
        printf("==============================================================================\n\n");
	}




	if ((numbytes=recv(sockfd, buf, MAXDATASIZE, 0)) == -1) {
		perror("recv");
		exit(1);
	}

	buf[numbytes] = '\0';



	printf("Received: %s",buf);

	close(sockfd);

	return 0;
}
