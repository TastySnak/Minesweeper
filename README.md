# Minesweeper
A project developing a minesweeper game on a client/server system, implemented in C using BSD sockets on the Linux operating system.


HOW TO COMPILE AND RUN THE PROGRAM

With the makefile in the same directory as the source files, compile them by  simply typing:

	$ make 

Executables for both client and server will be produced.
To get the server up and running in one terminal type:

	$ ./server 12345

This will have the server listening on port 12345, which is also the default port if the port number is omitted.
Clients can be opened in other terminals, with up to 10 connecting at once, when you type:

	$ ./client localhost 12345

This will make a connection with the server on the same port, after which you must authenticate in order to play the game.
Login credentials are located in the Authentication.txt file