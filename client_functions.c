#include "client.h"


/**********************************************/
/*             Global Variables               */
/**********************************************/

char username[AUTH_DATA_SIZE];


/**********************************************/
/*              Program Functions             */
/**********************************************/

/* Convert the validated corrdinates to array indices */
int convert_to_indices(char coord) {
        switch (coord) {
            case 'A':
            case 'a':
            case '1':
                return 0;
                break;
        
            case 'B':
            case 'b':
            case '2':
                return 1;
                break;
        
            case 'C':
            case 'c':
            case '3':
                return 2;
                break;
        
            case 'D':
            case 'd':
            case '4':
                return 3;
                break;
        
            case 'E':
            case 'e':
            case '5':
                return 4;
                break;
        
            case 'F':
            case 'f':
            case '6':
                return 5;
                break;
        
            case 'G':
            case 'g':
            case '7':
                return 6;
                break;
        
            case 'H':
            case 'h':
            case '8':
                return 7;
                break;
        
            case 'I':
            case 'i':
            case '9':
                return 8;
                break;
        }

    return RETURNED_ERROR;
}

/* Check entered coordinates for mine */
bool check_for_mine(char *results) {

    /* Check if tile has already been revealed */
    if (results[0] == 'R') {

        return true;
    }

    /* Check if tile contains a mine */
    if (results[1] == 'T') {

        /* Check if player has flagged all the mines */
        if (results[2] == 'N') {

            return false;
        }
    } else {
        printf("\nNo mine at this location!\n\n");
        
        pressEnter();
    }

    return true;
}

/* Validate user input of tile selection */
bool tile_option_valid(char *opt) {

    bool choice = false;

    if ((((opt[0] >= 'a') && (opt[0] <= 'i')) ||
        ((opt[0] >= 'A') && (opt[0] <= 'I'))) &&
        ((opt[1] >= '1') && (opt[1] <= '9'))) {
            choice = true;
        }

    if (!choice) {
        printf("\nInvalid option!\n");
        printf("\nThe first should be a character in the range [A - I]\n");
        printf("\nThe second should be a digit in the range [1 - 9]\n");
        printf("Enter tile coordinates: ");
    }

    return choice;
}

/* Lets server know the client is ready to receive more data */
void prompt_server(int sockfd) {
    if (send(sockfd, "a", 1, 0) == -1)
        perror("send");

    return;
}

/* Receives server's request to receive more data */
void receive_prompt(int sockfd) {
    
    char ch;
    int numbytes;
    
    if ((numbytes=recv(sockfd, &ch, 1, 0)) == -1) {
        perror("recv");
        exit(EXIT_FAILURE);
    }

    return;
}

/* Receives data from server to display on gameboard */
void print_game_board(int sockfd) {

    char ch, tiles[NUM_TILES_Y * NUM_TILES_X];
    int numbytes, mines_remaining;
    uint16_t mines;

    /* Receive number of remaining mines from server */
	if ((numbytes=recv(sockfd, &mines, sizeof(uint16_t), 0)) == -1) {
		perror("recv");
		exit(EXIT_FAILURE);
	}

	/* Convert to host byte order */
	mines_remaining = ntohs(mines);

    prompt_server(sockfd);
    
    /* Receive gameboard from server */
    for (int index = 0; index < (NUM_TILES_Y * NUM_TILES_X); index++) {
		if ((numbytes=recv(sockfd, &ch, 1, 0))
		         == RETURNED_ERROR) {
			perror("recv");
			exit(EXIT_FAILURE);			
		    
		}
		tiles[index] = ch;
	}

    system("clear");

    /* Display the game board */
    printf("\n\nRemaining mines: %d\n\n", mines_remaining);
    printf("        1   2   3   4   5   6   7   8   9\n");
    printf("============================================\n");
    printf("  A  || %c | %c | %c | %c | %c | %c | %c | %c | %c ||\n", 
            tiles[0], tiles[1], tiles[2], tiles[3], tiles[4], tiles[5], tiles[6], tiles[7], tiles[8]);
    printf("     ||-----------------------------------||\n");
    printf("  B  || %c | %c | %c | %c | %c | %c | %c | %c | %c ||\n",
            tiles[9], tiles[10], tiles[11], tiles[12], tiles[13], tiles[14], tiles[15], tiles[16], tiles[17]);
    printf("     ||-----------------------------------||\n");
    printf("  C  || %c | %c | %c | %c | %c | %c | %c | %c | %c ||\n",
            tiles[18], tiles[19], tiles[20], tiles[21], tiles[22], tiles[23], tiles[24], tiles[25], tiles[26]);
    printf("     ||-----------------------------------||\n");
    printf("  D  || %c | %c | %c | %c | %c | %c | %c | %c | %c ||\n", 
            tiles[27], tiles[28], tiles[29], tiles[30], tiles[31], tiles[32], tiles[33], tiles[34], tiles[35]);
    printf("     ||-----------------------------------||\n");
    printf("  E  || %c | %c | %c | %c | %c | %c | %c | %c | %c ||\n", 
            tiles[36], tiles[37], tiles[38], tiles[39], tiles[40], tiles[41], tiles[42], tiles[43], tiles[44]);
    printf("     ||-----------------------------------||\n");
    printf("  F  || %c | %c | %c | %c | %c | %c | %c | %c | %c ||\n", 
            tiles[45], tiles[46], tiles[47], tiles[48], tiles[49], tiles[50], tiles[51], tiles[52], tiles[53]);
    printf("     ||-----------------------------------||\n");
    printf("  G  || %c | %c | %c | %c | %c | %c | %c | %c | %c ||\n",
            tiles[54], tiles[55], tiles[56], tiles[57], tiles[58], tiles[59], tiles[60], tiles[61], tiles[62]);
    printf("     ||-----------------------------------||\n");
    printf("  H  || %c | %c | %c | %c | %c | %c | %c | %c | %c ||\n",
            tiles[63], tiles[64], tiles[65], tiles[66], tiles[67], tiles[68], tiles[69], tiles[70], tiles[71]);
    printf("     ||-----------------------------------||\n");
    printf("  I  || %c | %c | %c | %c | %c | %c | %c | %c | %c ||\n",
            tiles[72], tiles[73], tiles[74], tiles[75], tiles[76], tiles[77], tiles[78], tiles[79], tiles[80]);
    printf("============================================\n");

    return;
}

/* Game menu screen */
char game_menu(int sockfd) {

    char choice[2];

    printf("\n\nChoose an option:\n");
    printf("<R>	Reveal tile\n");
    printf("<P>	Place flag\n");
    printf("<Q>	Quit game\n\n");
    printf("Option (R, P, Q): ");

    do {
        read_user_input(choice, 2);
    } while (!game_option_valid(choice));

    /* Send choice to server's play_game() function */
	send(sockfd, &choice[0], 1, 0);

    return choice[0];
}

/* Quit game */
bool quit_game(int sockfd) {

    /* server is waiting for user input */
    receive_prompt(sockfd);
    
    printf("\n\nNever mind %s.  Better luck next time!\n\n", username);

    pressEnter();

    system("clear");

    return false;
}

/* place flag at designated coordinates if mine exists there */
bool place_flag(int sockfd) {
    char coords[3];
    uint16_t indices[2];
    char results[3];
    int numbytes, num_indices = 2, num_results = 3; 

    /* Prompt user for tile coordinates */
    printf("\nEnter tile coordinates: ");
    do {
        read_user_input(coords, 3);

    } while (!tile_option_valid(coords));

    /* Convert user input to array indices in network byte order */
    indices[0] = htons(convert_to_indices(coords[0]));
    indices[1] = htons(convert_to_indices(coords[1]));

    /* Send coordinates to server's place_flag() function */  
    for (int index = 0; index < num_indices; index++) {
		send(sockfd, &indices[index], sizeof(uint16_t), 0);
	}

    /* receive result from server */
    for (int index = 0; index < num_results; index++) {
		if ((numbytes=recv(sockfd, &results[index], 1, 0)) == -1) {
            perror("recv");
            exit(EXIT_FAILURE);
        }
	}

    /* Check tile for mine*/
    return check_for_mine(results);
}

/* Reveal the user selected tile */
bool reveal_tile(int sockfd) {
    char coords[3];
    uint16_t indices[2];
    char result;
    int numbytes, num_indices = 2;

    /* Prompt user for tile coordinates */
    printf("\nEnter tile coordinates: ");
    do {
        read_user_input(coords, 3);
    } while (!tile_option_valid(coords));

    /* Convert user input to array indices in network byte order*/
    indices[0] = htons(convert_to_indices(coords[0]));
    indices[1] = htons(convert_to_indices(coords[1]));

    /* Send coordinates to server's reveal_tile() function */  
    for (int index = 0; index < num_indices; index++) {
		send(sockfd, &indices[index], sizeof(uint16_t), 0);
	}

    /* receive result from server's display_results() function */
    if ((numbytes=recv(sockfd, &result, 1, 0)) == -1) {
        perror("recv");
        exit(EXIT_FAILURE);
    }

    /* Check the status of the tile */
    return display_results(result, sockfd);
}

/* Check the user's input against the tiles on the gameboard */
bool display_results(char result, int sockfd) {
    /* This is a recursive function */
    /* Check for base cases */

    /* Check for mine */
    if (result == 'T') {

        /* Prompt serrver for updated play screen */
        prompt_server(sockfd);
            
        /* Update play screen */
        print_game_board(sockfd);
        
        /* Display Loss */
        printf("\nYOU DETONATED A MINE!!\n");
        printf("Bad luck %s.  You lose!\n\n", username);

        pressEnter();

        system("clear");

        return false;
    } else {

        /* Prompt serrver for updated play screen */
        prompt_server(sockfd);
            
        /* Update play screen */
        print_game_board(sockfd);
    }

    return true;
}

/* Play Minesweeper */
void play_game(int sockfd) {

    /* Set up game */
    bool game_on = true;
    char game_choice;

    /* Do while game is in play */
    do {
        /* Update play screen */
        print_game_board(sockfd);

        /* Prompt user fromo game menu */
        game_choice = game_menu(sockfd);

        switch (game_choice) {
            case 'R':
            case 'r':
                game_on = reveal_tile(sockfd);
                break;

            case 'P':
            case 'p':
                game_on = place_flag(sockfd);

                /* Let server know the client is ready to receive the next lot of data */
                prompt_server(sockfd);
                break;

            case 'Q':
            case 'q':
                game_on = quit_game(sockfd);
                break;
        
            default:
                break;
        }
        
    } while (game_on);

    /* On win display victory screen */
    if (game_choice == 'P' || game_choice == 'p') {

        int numbytes, game_time;
        uint16_t g_time;

        /* Update play screen */
        print_game_board(sockfd);

        /* Get game time from server */
        prompt_server(sockfd);

        if ((numbytes=recv(sockfd, &g_time, sizeof(uint16_t), 0)) == -1) {
            perror("recv");
            exit(EXIT_FAILURE);
        }
    
	    /* Convert to host byte order */
        game_time = ntohs(g_time);

        /* Display Win */
        printf("\nCongratulations %s!  You have located all the mines.\n", username);
        printf("\nYou won in %d seconds!\n\n", game_time);

        pressEnter();

        system("clear");
    }
    
    return;
}

/* Press ENTER key to Continue */
void pressEnter() {

    printf("Press ENTER key to Continue\n");

    while (getchar() != '\n');

    return;
}

/* Display leaderboard */
void show_leaderboard(int sockfd) {
    int numbytes, node_count;
    uint16_t n_count;
    
	/* Receive number of nodes in the leaderboard from count_nodes() function */
	if ((numbytes=recv(sockfd, &n_count, sizeof(uint16_t), 0)) == -1) {
		perror("recv");
		exit(EXIT_FAILURE);
	}

	/* Convert to host byte order */
	node_count = ntohs(n_count);

    system("clear");

	if (node_count) {
        printf("\n\n=====================================================================\n");
        printf("=====================  Minesweeper Leaderboard  =====================\n");
        printf("=====================================================================\n\n");
		
        /* Print the sorted leaderboard */
        print_scores(node_count, sockfd);

        printf("\n=====================================================================\n\n");
        
	} else {
		
        printf("\n\n==============================================================================\n\n");
        printf("There is no information currently stored in the leaderboard.  Try again later.\n\n");
        printf("==============================================================================\n\n");
	}

    // pressEnter();   // For some reason 2 are needed 
    pressEnter();
    system("clear");

    return;
}

/* Print nodes in leaderboard */
void print_scores(int node_count, int sockfd) {
    int numbytes;

	for (int index = 0; index < node_count; index++) {

        /* signal to server's send_leaderboard() function that client is ready to receive */
        prompt_server(sockfd);
	    
        char buf[LEADER_DATA_SIZE];

		if ((numbytes=recv(sockfd, buf, LEADER_DATA_SIZE, 0)) == -1) {
            perror("recv");
            exit(EXIT_FAILURE);
        }

		char *token = strtok(buf, " "); 
		char  *name, *game_time, *games_played, *games_won;
		int counter = 0;

        // Keep printing tokens while one of the 
        // delimiters present in buf[]. 
        while (token != NULL) {
            
            switch (counter) {
                case 0:
                    RemoveSpaces(token);
                    name = token;
                    counter++;
                    break;
                case 1:
                    RemoveSpaces(token);
                    games_played = token;
                    counter++;
                    break;     
                case 2:
                    RemoveSpaces(token);
                    games_won = token;
                    counter++;
                    break;     
                case 3:
                    RemoveSpaces(token);
                    game_time = token;
                    counter++;
                    break;                
                default:
                    break;
            }

            token = strtok(NULL, " ");
        }

		printf(" %-10s %10s seconds %10s games won, %2s games played\n", 
                name, game_time, games_won, games_played);

	}

	return;
}

/* Removes the spaces from strings */
void RemoveSpaces(char* string) {
    int count = 0;

    for (int index = 0; string[index]; index++) {
        if (string[index] != ' ')
            string[count++] = string[index];
    }

    string[count] = '\0';

    return;
}

/* Exit program */
bool quit() {
    system("clear");

    printf("\n\nThank you %s for playing Minesweeper!\n", username);
    printf("See you again soon.\n\n");

    sleep(2);

    system("clear");

    return false;
}

/* Validate user input of main menu selection */
bool main_option_valid(int opt) {

    switch (opt) {
        case 1:
        case 2:
        case 3:
            return true;
        default :
            printf("\nInvalid option!\n");
            printf("Selection option (1 – 3): ");
    }
            
    return false;
}

/* Main menu screen */
int main_menu(int sockfd) {
	char ch;
	int choice;

    /* Clear the screen */
    system("clear");

    printf("\n\n===============================================\n");
    printf("Welcome to the online Minesweeper gaming system\n");
    printf("===============================================\n\n");
    printf("%s, please enter a selection:\n", username);
    printf("<1>	Play Minesweeper\n");
    printf("<2>	Show Leaderboard\n");
    printf("<3>	Quit\n\n");
    printf("Selection option (1 – 3): ");

    do {
        scanf("%d", &choice);

        // Clear the input buffer.
        while ((ch = getchar()) != '\n' && ch != EOF);

    } while (!main_option_valid(choice));

    /* Send player's selection to server's display_menu() function in network byte order */ 
    uint16_t int_choice = htons(choice);
	send(sockfd, &int_choice, sizeof(uint16_t), 0);

	return choice;
}

/* User sign in */
void sign_in_user(int sockfd) {
    
    char password[AUTH_DATA_SIZE];

    /* Initial clearing of the screen */
    system("clear");

    printf("\n\n===============================================\n");
    printf("Welcome to the online Minesweeper gaming system\n");
    printf("===============================================\n\n");
    printf("You are required to log on with your registered user name and password.\n\n");
    printf("Username: ");
    read_user_input(username, AUTH_DATA_SIZE);
    printf("\nPassword: ");
    read_user_input(password, AUTH_DATA_SIZE);

    /* Combine credentials to be sent at once */
    char * user_input = (char *) malloc(strlen(username) + strlen(password) + 2);  // +1 for ' ' and +1 for '\n'
    strcpy(user_input, username);
    strcat(user_input, " ");
    strcat(user_input, password);

    /* Send credentials */
    send_string(sockfd, user_input);

	/* Free memory */
	free(user_input);

    return;
}

/* Send a string of characters to the server */
void send_string(int sockfd, char *user_input) {

    if (send(sockfd, user_input, strlen(user_input), 0) == -1)
        perror("send");

    return;
}

/* Reads user input and clears input buffer */
/* Character array must have one extra element */
/* to take into account the null terminator character */
void read_user_input(char chars[], int maxlen) {
	char ch;
	int index = 0;
	bool chars_remain = true;

	while (chars_remain) {
		ch = getchar();
		if ((ch == '\n') || (ch == EOF) ) {
			chars_remain = false;
		} else if (index < maxlen - 1) {
			chars[index] = ch;
			index++;
		}
	}
	chars[index] = '\0';

	return;
}

/* Validate user input of game menu selection */
bool game_option_valid(char *opt) {

    switch (*opt) {
        case 'R':
        case 'P':
        case 'Q':
        case 'r':
        case 'p':
        case 'q':
            return true;
        default:
            printf("\nInvalid option!\n");
            printf("Please Choose 'R', 'P', or 'Q'\n\n");
    }

    return false;
}