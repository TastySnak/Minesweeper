#include "client.h"

/**********************************************/
/*              Main Function                 */
/**********************************************/

int main(int argc, char *argv[]) {
	int sockfd, numbytes, main_choice, in_play = 0;
    uint16_t recv_auth;
	struct hostent *he;
	struct sockaddr_in their_addr; /* connector's address information */
    bool auth_fail = false;

	if (argc != 3) {
		fprintf(stderr,"usage: %s <client_hostname> <portnumber>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

    /* get the host info */
	if ((he=gethostbyname(argv[1])) == NULL) {
		herror("gethostbyname");
		exit(EXIT_FAILURE);
	}

	/* generate the socket */
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(EXIT_FAILURE);
	}

	their_addr.sin_family = AF_INET;
    
	/* Convert to network byte order */
	their_addr.sin_port = htons(atoi(argv[2]));
	their_addr.sin_addr = *((struct in_addr *)he->h_addr);
	bzero(&(their_addr.sin_zero), 8);

	//Connect to remote server
	if (connect(sockfd, (struct sockaddr *)&their_addr, \
	sizeof(struct sockaddr)) == -1) {
		perror("connect");
		exit(EXIT_FAILURE);
	}

    /* User sign in */
    sign_in_user(sockfd);

    /* Receive message back from server */
	if ((numbytes=recv(sockfd, &recv_auth, sizeof(uint16_t), 0)) == -1) {
		perror("recv");
		exit(EXIT_FAILURE);
	}

	/* Convert to host byte order */
    if (ntohs(recv_auth)) {
	    in_play = ntohs(recv_auth);
    } else {
        auth_fail = true;
    }

	while (in_play) {
		main_choice = main_menu(sockfd);

		switch (main_choice) {
            case 1:
                play_game(sockfd);
                break;

            case 2:
                show_leaderboard(sockfd);
                break;

            case 3:
                in_play = quit();
                break;
        
            default:
                break;
        }

	}

	/* If login is unsuccessful, print before aborting */
	if (auth_fail) {
        system("clear");    

		printf("\nYou entered either an incorrect username or password.\nOr this user is already logged in!\nDisconnecting...\n");
		
        sleep(2);
        system("clear");
	}


	close(sockfd);

	return 0;
}

