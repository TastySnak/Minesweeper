#pragma once

#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <string.h> 
#include <netdb.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <unistd.h>
#include <arpa/inet.h>   // inet_addr
#include <stdbool.h>

#define AUTH_DATA_SIZE 10 /* used for authentication */
#define MENU_DATA_SIZE 2 /* used for menu options */
#define LEADER_DATA_SIZE 20 /* used for each leaderboard node received */

#define RETURNED_ERROR -1

#define NUM_TILES_X 9
#define NUM_TILES_Y 9
#define ROW 0
#define COLUMN 1


/**********************************************/
/*           Function declarations            */
/**********************************************/

void print_game_board(int);
int main_menu(int);
void read_user_input(char *, int);
bool main_option_valid(int);
bool quit();
void show_leaderboard(int);
void play_game(int);
bool game_option_valid(char *);
bool quit_game(int);
bool place_flag(int);
int convert_to_indices(char);
bool check_for_mine(char *);
bool tile_option_valid(char *);
bool reveal_tile(int);
bool display_results(char, int);
void sign_in_user(int);
void send_string(int, char *);
void RemoveSpaces(char *);
void print_scores(int, int);
void pressEnter();
void prompt_server(int);
void receive_prompt(int);