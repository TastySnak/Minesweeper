#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE

#include <stdlib.h> 
#include <stdio.h> 
#include <string.h> 
#include <arpa/inet.h>
#include <netinet/in.h> 
#include <stdbool.h>
#include <pthread.h>
#include <signal.h>
#include <errno.h> 

#include "server.h"


/**********************************************/
/*              Global variables              */
/**********************************************/

board_t *scoreboard = NULL;
client_t *creds_list = NULL;
players_t *players_list = NULL;
games_t *games_list = NULL;
request_t *requests = NULL;
request_t *last_request = NULL;

pthread_mutex_t rand_mutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
pthread_mutex_t read_count_mutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
pthread_mutex_t rw_leader_mutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
pthread_mutex_t players_mutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
pthread_mutex_t games_mutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
pthread_mutex_t creds_mutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
pthread_mutex_t threads_mutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;

pthread_attr_t t_attr[BACKLOG];
pthread_t p_threads[BACKLOG];

int port_number = 12345,
    read_count = 0,
    num_requests = 0,
    new_fd, sockfd;

/* global mutex for main. assignment initializes it. */
pthread_mutex_t request_mutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
/* global mutexes for our program. assignment initializes it. */

/* global condition variable for our program. */
pthread_cond_t got_request = PTHREAD_COND_INITIALIZER;


/**********************************************/
/*              Main Function                 */
/**********************************************/

int main(int argc, char *argv[]) {

	struct sockaddr_in my_addr, their_addr;
	socklen_t sin_size;

    /* Seed the random number generator */
    srand(RANDOM_NUMBER_SEED);

    /* signal() system call on Ctrl-C */
    signal(SIGINT, sigHandler);
    signal(SIGHUP, sigHandler);
    
    /* Parse user credentials from file to list */
    creds_list = parse_authentication_file(creds_list);

	/* generate the socket */
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(1);
	}

    /* Assign port number */
    if (argc > 1)
        port_number = atoi(argv[1]);

	/* generate the end point */
	my_addr.sin_family = AF_INET;

	/* Convert to network byte order */
	my_addr.sin_port = htons(port_number);
	my_addr.sin_addr.s_addr = INADDR_ANY;
    memset(&(my_addr.sin_zero), 0, sizeof(my_addr.sin_zero));
    
	/* bind the socket to the end point */
	if (bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) \
	== -1) {
		perror("bind");
		exit(1);
	}

	/* start listnening */
	if (listen(sockfd, BACKLOG) == -1) {
		perror("listen");
		exit(1);
	}

    printf("Server is listening on port %d \n", port_number);
    
    /* create the request-handling threads */
    for (int index = 0; index < BACKLOG; index++) {
        pthread_attr_init((&t_attr[index]));
        pthread_attr_setdetachstate(&t_attr[index],PTHREAD_CREATE_DETACHED);
        pthread_create(&p_threads[index], &t_attr[index], handle_requests_loop, NULL);
    }

    /* main accept() loop */
    while(true) {

        sin_size = sizeof(struct sockaddr_in);

        if ((new_fd = accept(sockfd, (struct sockaddr *)&their_addr, \
        &sin_size)) == -1) {
            perror("accept");
            continue;
        }

        printf("server: got connection from %s\n", \
            inet_ntoa(their_addr.sin_addr));

        add_request(new_fd, &request_mutex, &got_request);     
    }

    /* Just in case the main loop breaks for some reason */
    graceful_shutdown();

    return EXIT_SUCCESS; 
}


